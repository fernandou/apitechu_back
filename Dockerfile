#Image Docker base/inicial
FROM node:latest

# crear directorio del contenedor Docker
WORKDIR  /docker-apitechu

#copia de archivos del proyecto en el directorio del contenedor
ADD . /docker-apitechu

#Exponer puerto del contenedor (mismo definido en nuestra API)
EXPOSE 3000

#lanzar comando para ejecutar nuestra app
CMD ["npm", "run", "start-pro"]
